const http = require("http");
const port = 4000;

http.createServer( (request, response) => {

    if (request.url == "/items" && request.method == "GET") {

        // HTTP method of the incoming request can be accessed via the method property of the request parameter
        // The method GET means that we will be retrieving or reading an information
        response.writeHead(200, {"Content-Type":"text/plain"});
        response.end(`Data retrieved from the database`);
    } else if (request.url == "/items" && request.method == "POST") {

        // Request the '/items' path and "SENDS" information
        response.writeHead(200, {"Content-Type":"text/plain"});
        response.end(`Data to be sent to the database`);
    }
    
}).listen(4000);

console.log(`Server running at localhost:${port}`);