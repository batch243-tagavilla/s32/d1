const http = require("http");
const port = 4000;

// Mock database
let directory = [
    {
        name: "Brandon",
        email: "brandon@mail.com",
    },
    {
        name: "Jobert",
        email: "jobert@mail.com",
    },
];

const server = http
    .createServer((request, response) => {
        // Route for returning all items upon receiving a GET request
        if (request.url == "/users" && request.method == "GET") {
            response.writeHead(200, { "Content-Type": "application/json" });
            response.write(JSON.stringify(directory)); // input has be data type STRING hence the JSON.stringify() method
            response.end();
        }

        // add user to the database
        if (request.url == "/users" && request.method == "POST") {
            // declare and initialize a "requestBody" variable to an empty string
            // this will act as a placeholder for the data to be created later on
            let requestBody = "";

            // a stream is a sequence of data
            // data is received from the client and is processed in the "data stream"
            // the information provided from the request object eneters a sequence called "data" the colde below will be triggered
            // data strp - this reads the "data" stream and process it as the request body
            request.on("data", (data) => {
                requestBody += data;
            });

            // response end step - it only runs after the request has completely been sent
            request.on("end", () => {
                console.log(typeof requestBody);

                requestBody = JSON.parse(requestBody);

                // create a new object representing the new mock database record
                const newUser = {
                    name: requestBody.name,
                    email: requestBody.email,
                };

                // add the new user into the mock database
                directory.push(newUser);
                console.log(directory);
            });

            response.writeHead(200, { "Content-Type": "application/json" });
            response.write(JSON.stringify(directory));
            response.end();
        }
    })
    .listen(4000);

console.log(`Server running at localhost:${port}`);
